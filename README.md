# grafana-proxy
grafana提供了功能强大的监控图表功能，我们希望将grafana与业务系统集中，并且希望能够自定义grafana dashboard的页面内容。



# 实现方案
- 采用zuul作为grafana的代理组件，在grafana返回dashboard页面内容时，动态插入JavaScript文件，通过JavaScript修改页面来实现。
- 这种方案不需要修改grafana源码，就可以实现快速,有效的定制。

# 使用
## 1. 启动grafana，并且配置grafana允许匿名访问

docker run -d --name=grafana  -v /etc/grafana/grafana.ini:/etc/grafana/grafana.ini -v /var/lib/grafana:/var/lib/grafana -p 3000:3000 grafana/grafana:4.6.2

![grafana.ini配置文件示例](grafana.ini)


grafana如何配置匿名访问，请参考 [http://https://my.oschina.net/weidedong/blog/759725](http://https://my.oschina.net/weidedong/blog/759725)


## 2. 在grafana中创建一个d1的dashboard，通过grafana直接访问d1 dashboard

http://192.168.1.6:3000/dashboard/db/dashboard?orgId=1

![multisources](img/1.png)


## 3. 部署grafana-proxy

grafana-proxy的配置文件为application.yml，修改grafana-server参数

grafana-server: http://192.168.1.6:3000


## 4. 通过grafana-proxy访问d1 dashboard

http://localhost:8800/dashboard/db/d1?orgId=1

![multisources](img/2.png)


## 5. 这个就完成对grafana dashboard的修改。配合grafana的template variables功能，可以更多高级功能。
