(function () {

    $(function () {
        // 不能使用hide()方法
        $(".main-view").css("visibility", "hidden");
    });

    function isload() {
        if ($(".main-view .pull-left").length > 0) {
            var $main = $(".main-view");
            $(".main-view").css("visibility", "visible");
            $main.find(".pull-left").hide();

            $("<span style='padding:15px;display: inline-block;' id='cloudresource_name'>我们修改了dashboard的内容</span>").insertAfter($main.find(".pull-left"));

            $main.find(".navbar-section-wrapper").hide();
            $main.find(".navbar-brand-btn").hide();
            $main.find("dashboard-submenu").hide();

            return;
        }

        setTimeout(isload, 1000);
    }

    setTimeout(isload, 1000);

})();
